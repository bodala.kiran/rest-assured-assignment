package Assignment;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.Validatable;
import io.restassured.response.ValidatableResponse;

public class GoRestAssignment {
	@Test(priority = 0)
    public void testStatusCode() {
        baseURI = "https://gorest.co.in/";
        given().get("public/v2/users").then().statusCode(200);
    }
    @Test(priority = 1)
    public void firstGetMethod() {
        Response res = RestAssured.get("https://gorest.co.in/public/v2/users");
        int statusCode = res.getStatusCode();
        System.out.println(statusCode);
        System.out.println(res.getBody().asString());
    }
    @Test(priority = 2)
    private void assertGetMethod() {
        Response res = RestAssured.get("https://gorest.co.in/public/v2/users");
        int statusCode = res.getStatusCode();
        Assert.assertEquals(200, statusCode);
    }
    @Test(priority = 3)
    public void postOperationTest() {
        JSONObject req = new JSONObject();
        req.put("name", "Sai");
        req.put("email", "saikiranbodala@gmail.com");
        req.put("gender", "male");
        req.put("status", "Active");
        System.out.println(req);
        baseURI = "https://gorest.co.in/public/v2";
    ValidatableResponse res = given().log().all().contentType("application/json")
                .header("authorization", "Bearer dad43fac2bd5aafcb039e7b1c10923290a4a6ea15dc61349afc621e8c55a2994")
                .body(req.toJSONString()).when().post("/users").then().statusCode(201);
        
    }
    @Test(priority = 4)
    public void putOperationTest() {
        JSONObject req = new JSONObject();
        req.put("name", "Kiran");
        req.put("email", "saikiranbodala@gmail.com");
        req.put("gender", "male");
        req.put("status", "Active");
        System.out.println(req);
        baseURI = "https://gorest.co.in/public/v2";
        given().log().all().contentType("application/json")
                .header("authorization", "Bearer dad43fac2bd5aafcb039e7b1c10923290a4a6ea15dc61349afc621e8c55a2994")
                .body(req.toJSONString()).when().put("/users/12345").then()
                .statusCode(200);
    }
    @Test(priority = 5)
    public void DeleteOperationTest() {
        baseURI = "https://gorest.co.in/public/v2";
        given().log().all().contentType("application/json")
                .header("authorization", "Bearer dad43fac2bd5aafcb039e7b1c10923290a4a6ea15dc61349afc621e8c55a2994")
                .when().delete("/users/12345").then().statusCode(204);
    }
}
