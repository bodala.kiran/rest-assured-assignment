package APItesting;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class TestingDeleteOperation {
  @Test
  public void deleteOperation() {
	  baseURI="https://reqres.in/";
	  given().when().delete("api/users/2").then().statusCode(204).log().all();
  }
}
