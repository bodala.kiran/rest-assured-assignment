package APItesting;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class TestingPatchRequest {
  @Test
  public void updatingDataUsingPatch() {
	
		  JSONObject request=new JSONObject();
		  request.put("name", "kiran");
		  request.put("job", "trainer");
		  
		  given().body(request.toJSONString()).patch("https://reqres.in/api/users/2").then().statusCode(200)
		 .body("updatedAt", greaterThanOrEqualTo("2023-01-24T10:11:12.176Z"));
	  }
  }

