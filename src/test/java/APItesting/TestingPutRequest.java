package APItesting;

import org.json.simple.JSONObject;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import org.testng.annotations.Test;

public class TestingPutRequest {
  @Test
  public void updatingDataUsingPut() {
	  JSONObject request=new JSONObject();
	  request.put("name", "kiran");
	  request.put("job", "trainer");
	  
	  given().body(request.toJSONString()).put("https://reqres.in/api/users/2").then().statusCode(200)
	 .body("updatedAt", greaterThanOrEqualTo("2023-01-24T10:12:40.523Z"));
  }
}
